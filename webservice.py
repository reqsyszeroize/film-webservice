from flask import Flask, jsonify, request, redirect

webservice = Flask(__name__)

# Liste de films stockés en mémoire
films = []

# Route de redirection de / vers /films
@webservice.route('/')
def index():
    return redirect('/films')


# Route pour récupérer tous les films
@webservice.route('/films', methods=['GET'])
def get_films():
    return jsonify(films)

# Route pour récupérer un film par son ID
@webservice.route('/films/<int:film_id>', methods=['GET'])
def get_film(film_id):
    # Recherche du film par son ID
    film = next((film for film in films if film.get('id') == film_id), None)
    if film:
        return jsonify(film)
    else:
        return jsonify({'message': 'Film introuvable !'}), 404

# Route pour créer un nouveau film
@webservice.route('/films', methods=['POST'])
def create_film():
    data = request.get_json()

    # Vérification des champs obligatoires
    required_fields = ['nom', 'description', 'date_parution']
    if not all(field in data for field in required_fields):
        return jsonify({'message': 'Champs obligatoires manquants !'}), 422

    # Création d'un nouveau film et ajout à la liste de films
    new_film = {
        'id': len(films) + 1,
        'nom': data['nom'],
        'description': data['description'],
        'date_parution': data['date_parution'],
        'note': data.get('note', None)
    }
    films.append(new_film)
    return jsonify(new_film), 201

# Route pour mettre à jour un film existant
@webservice.route('/films/<int:film_id>', methods=['PUT'])
def update_film(film_id):
    data = request.get_json()

    # Vérification des champs obligatoires
    required_fields = ['nom', 'description', 'date_parution']
    if not all(field in data for field in required_fields):
        return jsonify({'message': 'Champs obligatoires manquants'}), 422

    # Recherche du film par son ID
    film = next((film for film in films if film.get('id') == film_id), None)
    if film:
        # Mise à jour des données du film
        film.update(data)
        return jsonify(film), 200
    else:
        return jsonify({'message': 'Film introuvable !'}), 404

# Route pour supprimer un film par son ID
@webservice.route('/films/<int:film_id>', methods=['DELETE'])
def delete_film(film_id):
    global films
    # Suppression du film de la liste de films
    films = [film for film in films if film.get('id') != film_id]
    return jsonify({'message': 'Film supprime !'}), 200

if __name__ == '__main__':
    webservice.run(debug=True)
