# WebService Movie

Cette API permet d'ajouter, de récupérer, de mettre à jour & supprimer des films.

## Installation et exécution

- Assurez-vous d'avoir Python installé.
- Installation des dépendances via `pip install Flask`.
- Execution de l'application via `python webservice.py`.

## Routes

### Récupération de tous les films

- URL : `/films`
- Méthode : `GET`
- Description : Cette route renvoie la liste de tous les films enregistrés.

### Récupération d'un film spécifique par son ID

- URL : `/films/<film_id>`
- Méthode : `GET`
- Description : Cette route renvoie les détails d'un film spécifique en utilisant son ID.

### Création d'un nouveau film

- URL : `/films`
- Méthode : `POST`
- Description : Cette route permet d'ajouter un nouveau film à la liste.

### Mise à jour d'un film existant

- URL : `/films/<film_id>`
- Méthode : `PUT`
- Description : Cette route permet de mettre à jour les détails d'un film existant en utilisant son ID.

### Suppression d'un film par son ID

- URL : `/films/<film_id>`
- Méthode : `DELETE`
- Description : Cette route permet de supprimer un film spécifique en utilisant son ID.
